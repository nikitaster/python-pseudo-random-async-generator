### Installation

```bash
git clone https://gitlab.com/nikitaster/python-pseudo-random-async-generator.git
cd python-pseudo-random-async-generator

pip3 install -r requirements.txt
```

### Run
```python
python generator.py
```
Or
```python
python generator.py --files_amount 100
python generator.py --files_size 10000
python generator.py --files_amount 100 --files_size 10000
```

**--files_amount** is amount of files. Require **integer**

**--files_size** is size of every file in bytes. Require **integer** - amount of bytes
