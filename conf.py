"""Config file"""

RANDOM_FOLDER = 'random'
URANDOM_FOLDER = 'urandom'
DESCRIPTION = 'Generator of the pseudo-random sequence of byte numbers using urandom, random'
FILES_AMOUNT = 10000
# FILES_SIZE = 2**22 // 8
FILES_SIZE = 16384 * 1024  # 16384Kb

