"""Generator of the pseudo-random sequence of byte numbers using urandom, random"""

import os
import asyncio
import argparse
import random
import datetime
from aiofile import async_open
from conf import RANDOM_FOLDER, URANDOM_FOLDER, DESCRIPTION, FILES_AMOUNT, FILES_SIZE


class DoneController:
    """Used for checking done of the coroutines"""

    def __init__(self, need_to_stop=2):
        self.need_to_stop = need_to_stop
        self.count = 0

    def add_complete(self):
        self.count += 1
        return self

    def check_done(self):
        if self.count >= self.need_to_stop:
            return True
        return False


def parse_args():
    """This function is used for get files amount and files size from args"""

    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--files_amount', type=int,
                        help='Amount of the generating files')
    parser.add_argument('--files_size', type=int,
                        help='Size of the one file in bytes')
    args = parser.parse_args()

    return args.files_amount if args.files_amount else FILES_AMOUNT, args.files_size * 8 if args.files_size else FILES_SIZE


def get_random_value() -> int:
    """Returns 1 random byte using Mersenne Twister algorithm"""
    return random.getrandbits(8)


def get_urandom_value() -> int:
    """Returns 1 random byte using OS built-in algorithm"""
    return int.from_bytes(os.urandom(1), byteorder='little')


async def process_random(random_method, folder: str, file_number: int,
                         files_size: int, controller: DoneController) -> None:
    """Async writing files"""

    filename = 'simon_{file_number}.txt'.format(
        type=folder,
        timepoint=datetime.datetime.now().strftime('%Y%m%d_%H%M%S'),
        file_number=file_number + 1
    )
    filepath = os.path.join(folder, filename)
    
    if os.path.exists(filepath):
        async with async_open(filepath, 'a') as afp:
            while os.path.getsize(filepath) < files_size:
                await afp.write('{decimal:02x}'.format(decimal=random_method()))
    else:
        async with async_open(filepath, 'w+') as afp:
            while os.path.getsize(filepath) < files_size:
                await afp.write('{decimal:02x}'.format(decimal=random_method()))

    controller.add_complete()


async def check_done_loop(controller: DoneController) -> None:
    """Check done for exiting the script"""

    while True:
        if controller.check_done():
            asyncio.get_running_loop().stop()
            print('DONE')
            return
        await asyncio.sleep(60)
        

async def start_random(f_amount, f_size, done_controller):
    for f_number in range(1, f_amount + 1, 1):
        await asyncio.ensure_future(process_random(
            get_random_value, RANDOM_FOLDER, f_number, f_size, done_controller))
        print('random {} is done'.format(f_number))
            
async def start_urandom(f_amount, f_size, done_controller):
    for f_number in range(1, f_amount + 1, 1):
        await asyncio.ensure_future(process_random(
            get_urandom_value, URANDOM_FOLDER, f_number, f_size, done_controller))
        print('urandom {} is done'.format(f_number))


if __name__ == '__main__':
    os.makedirs(RANDOM_FOLDER, exist_ok=True)
    os.makedirs(URANDOM_FOLDER, exist_ok=True)

    f_amount, f_size = parse_args()
    done_controller = DoneController(f_amount*2)

    loop = asyncio.get_event_loop()

    asyncio.ensure_future(start_random(f_amount, f_size, done_controller))
    asyncio.ensure_future(start_urandom(f_amount, f_size, done_controller))
    asyncio.ensure_future(check_done_loop(done_controller))

    loop.run_forever()