import hashlib
import random
import string
import os
import random
import datetime
from aiofile import async_open
from conf import RANDOM_FOLDER, URANDOM_FOLDER, DESCRIPTION, FILES_AMOUNT, FILES_SIZE


def get_random_value() -> int:
    """Returns 1 random byte using Mersenne Twister algorithm"""
    return random.getrandbits(8)

for j in range(800):
    filepath = os.path.join('random_sync', 'simon_{}.txt'.format(j+201))

    with open(filepath, 'w+') as f:
        while os.path.getsize(filepath) < FILES_SIZE:
            f.write('{decimal:02x}'.format(decimal=get_random_value()))
